@ECHO OFF

SETLOCAL enableextensions enabledelayedexpansion

SET F_P=%ROOT_PATH_BATCH_FILE_FULL_PATH%
if [!F_P!] == [] (
	Set F_N=_GetWorkspaceRootPath.bat
	Set F_P=!F_N!
)
Set F_P=..\!F_P!
if not exist !F_P! (
	echo !F_P! does not exist.
	pause
    exit /B
)
ENDLOCAL&(
	SET ROOT_PATH_BATCH_FILE_FULL_PATH=%F_P%
)
REM echo %ROOT_PATH_BATCH_FILE_FULL_PATH%
call %ROOT_PATH_BATCH_FILE_FULL_PATH%

REM echo %AZ_WORKSPACE_ROOT_PATH%
REM echo %AZ_7Z_COMMAND_LINE%
REM pause
