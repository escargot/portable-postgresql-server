@ECHO OFF

set TOOLS_ROOT=%~dp0
set SOFTWARE_ROOT=%TOOLS_ROOT%\pgsql\bin

title "pgAdmin III"

start %SOFTWARE_ROOT%\pgAdmin3.exe
exit
