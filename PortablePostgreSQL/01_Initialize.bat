@ECHO OFF

set TOOLS_ROOT=%~dp0
set POSTGRESQL_ROOT=%TOOLS_ROOT%\pgsql

PATH=%POSTGRESQL_ROOT%\bin;%PATH%

title "PostgreSQL"

@ECHO ON
REM The script sets environment variables helpful for PostgreSQL
@SET PATH="%POSTGRESQL_ROOT%\bin";%PATH%
@SET PGDATA=%POSTGRESQL_ROOT%\data
@SET PGDATABASE=postgres
@SET PGUSER=postgres
@SET PGPORT=5439
@SET PGLOCALEDIR=%POSTGRESQL_ROOT%\share\locale
REM Next line MUST be uncommented the first time to init the server, it can then be commented.

REM "%POSTGRESQL_ROOT%\bin\initdb.exe" -U postgres -A trust
"%POSTGRESQL_ROOT%\bin\initdb.exe" -U postgres -A password -W

@ECHO OFF

break > pgsql\AZ_PostgreSQL_Setup_Done.txt

echo.
echo.
echo Setup Completed Successfully !!!
echo.
echo Now you can keep runing "Run PostgreSQL.bat" to run the service...
echo.
echo.

pause




REM xcopy "%tbSrcDir%" "%tbDestRootDir%" /S /Y /I

REM if errorlevel 1 (
REM		echo Copy %tbSrcDir% to %tbDestRootDir%  failed !
REM 	pause
REM 	exit /B
REM )