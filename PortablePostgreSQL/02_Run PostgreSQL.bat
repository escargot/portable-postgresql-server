@ECHO OFF

set TOOLS_ROOT=%~dp0
set POSTGRESQL_ROOT=%TOOLS_ROOT%\pgsql

PATH=%POSTGRESQL_ROOT%\bin;%PATH%

title "PostgreSQL"

REM Setup Port

call PortDefinition.bat

if defined AZ_POSTGRESQL_PORT (
	goto run_service
)

set AZ_POSTGRESQL_PORT = 5432

:run_service

echo /////////////////////////////////////////////////////
echo service port = %AZ_POSTGRESQL_PORT%
echo /////////////////////////////////////////////////////


REM The script sets environment variables helpful for PostgreSQL

@ECHO ON

@SET PATH="%POSTGRESQL_ROOT%\bin";%PATH%
@SET PGDATA=%POSTGRESQL_ROOT%\data
@SET PGDATABASE=postgres
@SET PGUSER=postgres
@SET PGPORT=%AZ_POSTGRESQL_PORT%
@SET PGLOCALEDIR=%POSTGRESQL_ROOT%\share\locale

@ECHO OFF

REM "%POSTGRESQL_ROOT%\bin\pg_ctl.exe" -o "-F -p 5439" -D "%POSTGRESQL_ROOT%/data" -l %POSTGRESQL_ROOT%/logfile start

@ECHO ON
"%POSTGRESQL_ROOT%\bin\pg_ctl.exe" -D "%POSTGRESQL_ROOT%/data" -l %POSTGRESQL_ROOT%/logfile start
@ECHO OFF
ECHO.
ECHO.
ECHO "Click enter to stop"
ECHO.
ECHO.

pause

ECHO.
ECHO.
@ECHO ON
"%POSTGRESQL_ROOT%\bin\pg_ctl.exe" -D "%POSTGRESQL_ROOT%/data" stop


