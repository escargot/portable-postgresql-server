@ECHO OFF

Set ROOT_PATH_BATCH_FILE_NAME=_GetWorkspaceRootPath.bat
Set ROOT_PATH_BATCH_FILE_FULL_PATH=.\%ROOT_PATH_BATCH_FILE_NAME%

if not exist %ROOT_PATH_BATCH_FILE_FULL_PATH% (
	echo %ROOT_PATH_BATCH_FILE_NAME% does not exist.
	pause
    exit /B
)

call %ROOT_PATH_BATCH_FILE_FULL_PATH%

if not defined AZ_WORKSPACE_ROOT_PATH (
	echo AZ_WORKSPACE_ROOT_PATH is not defined !
	pause
    exit /B
)

if not defined AZ_7Z_COMMAND_LINE (
	echo AZ_7Z_COMMAND_LINE is not defined !
	pause
    exit /B
)

if not exist pgsql (
	goto extracting_file
)

SET /P ANSWER=Do you want to delete %CD%\pgsql [Y/N] ?
REM echo You chose: %ANSWER% 
if /i {%ANSWER%}=={y} (goto :yes) 
if /i {%ANSWER%}=={yes} (goto :yes) 


goto :no 

:yes

echo Deleting %CD%\pgsql ...

rmdir /s/q pgsql
REM echo You pressed yes! 
goto extracting_file

:no
REM echo You pressed no!
pause
exit /b 1



:extracting_file


echo Extracting file(s)...
%AZ_7Z_COMMAND_LINE% x "postgresql-*-windows*.zip" >nul 2>&1
REM %AZ_7Z_COMMAND_LINE% x "postgresql-*-windows-binaries.zip" >nul 2>&1
REM %AZ_7Z_COMMAND_LINE% x "postgresql-9.2.8-4-windows-binaries.zip"

echo.
echo.
echo Extract Completed Successfully !!!
echo.
echo Now you can keep runing "Initialize.bat" to initialize the environment...
echo.
echo.

pause