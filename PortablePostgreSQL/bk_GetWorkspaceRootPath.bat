@ECHO OFF

Set ROOT_PATH_BATCH_FILE_NAME=_AZ_WorkspaceRootPath.bat
Set ROOT_PATH_BATCH_FILE_FULL_PATH=..\..\%ROOT_PATH_BATCH_FILE_NAME%

if not exist %ROOT_PATH_BATCH_FILE_FULL_PATH% (
	echo %ROOT_PATH_BATCH_FILE_NAME% does not exist.
	pause
    exit /B
)

call %ROOT_PATH_BATCH_FILE_FULL_PATH%
