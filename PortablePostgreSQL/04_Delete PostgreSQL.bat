@ECHO OFF

if not exist pgsql (
	goto end
)

SET /P ANSWER=Do you want to delete %CD%\pgsql [Y/N] ?
REM echo You chose: %ANSWER% 
if /i {%ANSWER%}=={y} (goto :yes) 
if /i {%ANSWER%}=={yes} (goto :yes) 


goto :no 

:yes

echo Deleting %CD%\pgsql ...

rmdir /s/q pgsql
REM echo You pressed yes! 
goto end

:no
REM echo You pressed no!
pause
exit /b 1


:end


echo.
echo.
echo Delete Completed Successfully !!!
echo.
echo.

pause