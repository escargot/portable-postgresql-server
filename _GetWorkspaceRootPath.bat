@ECHO OFF

REM Root Path
set AZ_WORKSPACE_ROOT_PATH=%~dp0

set AZ_7Z_COMMAND_LINE=%AZ_WORKSPACE_ROOT_PATH%_AZ_SetupTools\7za920\7za.exe

if not exist %AZ_7Z_COMMAND_LINE% (
	set AZ_7Z_COMMAND_LINE=
	echo %AZ_7Z_COMMAND_LINE% does not exist.
	pause
    exit /B
)